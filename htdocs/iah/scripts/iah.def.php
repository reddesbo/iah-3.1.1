<?php
[PATH]

PATH_DATA=/iah/
PATH_CGI-BIN=/bvs/www/htdocs/iah/scripts/
PATH_DATABASE=/bvs/www/bases/
PATH_DEF=/bvs/www/bases/par/

[APPEARANCE]

/* Please adjust /css/stylesheet.css */

[HEADER]

LOGO IMAGE=bvs.gif
LOGO URL=^1http://www.bvs.br/^2http://www.bvsalud.org/^3http://www.virtualhealthlibrary.org/^4http://www.virtualhealthlibrary.org
HEADER IMAGE=online.gif
HEADER URL=^1/iah/pt/index.htm^2/iah/es/index.htm^3/iah/en/index.htm^4/iah/fr/index.htm

[IAH]

MANAGER E-MAIL=iah@bireme.br
REVERSE MODE=ON
MULTI-LANGUAGE=ON
AVAILABLE LANGUAGES=pt, es, en, fr


?>
